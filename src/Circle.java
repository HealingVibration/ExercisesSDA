public class Circle {
    public float d;

    public Circle(float d) {
        this.d = d;
    }

    public float circuit() {
        float circuit;

        circuit = 2 * (float) Math.PI * (this.d / 2);

        return circuit;
    }

}
