public class BMI {
    float weight;
    int height;

    public BMI(float weight, int height) {
        this.weight = weight;
        this.height = height;
    }

    public void checkBMI() {
        float calculatorBMI;
        calculatorBMI = this.weight /(( (float) this.height/100)* ((float) this.height/100));
        if (calculatorBMI > 18.5 && calculatorBMI < 24.9) {
            System.out.println("BMI optymalne");
        } else {
            System.out.println("BMI nieoptymalne");
        }
        System.out.println(calculatorBMI);
    }

}
