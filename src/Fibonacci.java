public class Fibonacci {

    public static void fibonacci(int number){
        int[] array = new int[number+1];
        array[0] = 1;
        array[1] = 1;
        for(int i = 2; i <number; i++){
            array[i] = array[i-2] + array[i-1];
        }
        System.out.println(array[number-1]);
    }

}
