import java.util.Scanner;

public class LiveCoding {

    public static void main(String[] args) {
        Circle circle = new Circle(30F);
        BMI myBMI = new BMI(70F, 175);
        RównanieKwadratowe fx = new RównanieKwadratowe(5, 10, -5);
        Calculator calculator = new Calculator();
        Wave wave = new Wave();
        Zadanie10 zad10 = new Zadanie10();

        Scanner scanner = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        calculator.setNumber(scanner.nextFloat());
        calculator.setZnak(sc.nextLine());
        calculator.setNumber2(scanner.nextFloat());

        System.out.println(circle.circuit());
        myBMI.checkBMI();
        fx.delta();
        if (fx.delta >= 0) {
            System.out.println("x1 = " + fx.x1());
            System.out.println("x2 = " + fx.x2());
        } else {
            System.out.println("delta ujemna");
            System.exit(0);
        }

        Zadanko4.wypisz(60);
        FirstNumbers.firstNumbers(15);
        Ciąg.ciąg(43);
        Fibonacci.fibonacci(10);
        calculator.calculator(calculator.getNumber(),calculator.getNumber2(),calculator.getZnak());
        wave.wave(7);
        zad10.sumOfDigits(789);
    }
}
