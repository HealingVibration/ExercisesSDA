public class Zadanie10 {


    public void sumOfDigits(int number) {
        int sum = 0;
        while (number != 0) {
            sum = number % 10 + sum;
            number = number / 10;
        }
        System.out.println(sum);
    }


}
