public class RównanieKwadratowe {

    double a, b, c;
    double delta;

    public RównanieKwadratowe(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;

    }

    public double delta() {
        delta = (b * b) - (4 * a * c);
        return delta;
    }

    public double x1() {
        double x1;

        x1 = (b * (-1)) - Math.sqrt(delta);
        return x1;
    }

    public double x2() {
        double x2;
        x2 = (b - b * 2) + Math.sqrt(delta);
        return x2;
    }

}
