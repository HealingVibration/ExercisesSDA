import java.util.Scanner;

public class Calculator {

    float number;
    float number2;
    String znak;


    public Calculator() {

    }

    public void setNumber(float number) {
        this.number = number;
    }

    public void setNumber2(float number2) {
        this.number2 = number2;
    }

    public void setZnak(String znak) {
        this.znak = znak;
    }

    public float getNumber() {
        return number;
    }

    public float getNumber2() {
        return number2;
    }

    public String getZnak() {
        return znak;
    }

    public void calculator(float number, float number2, String znak) {
        float wynik;
        switch (znak) {
            case "+":
                wynik = number + number2;
                System.out.println(wynik + " dodawanie");
                break;
            case "-":
                wynik = number - number2;
                System.out.println(wynik + " odejmowanie");
                break;

            case "*":
                wynik = number * number2;
                System.out.println(wynik + " mnożenie");
                break;
            case "/":
                if(number2 == 0 ) {
                    System.out.println("błąd");
                    break;
                }
                wynik = number / number2;
                System.out.println(wynik + " dzielenie");
                break;
            default:
                System.out.println("błędny znak");
                break;
        }


    }


}
